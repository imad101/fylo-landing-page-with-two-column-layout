var formInput = document.querySelector(".form");
var emailReg = /^([A-Za-z0-9_\-\.]) +\@([A-Za-z0-9_\-\.]) +\.([A-Za-z]{2,4})$/;
var error = document.querySelector("[data-error]");
formInput.addEventListener("submit", formHandler);
function formHandler(e) {
    e.preventDefault();
    if (e.target.value === "" || !emailReg.test(e.target.value)) {
        error.setAttribute("data-error", "true");
    }
    else {
        error.setAttribute("data-error", "false");
    }
}
