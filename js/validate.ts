const formInput: HTMLFormElement = document.querySelector(".form")
const emailReg: RegExp =  /^([A-Za-z0-9_\-\.]) +\@([A-Za-z0-9_\-\.]) +\.([A-Za-z]{2,4})$/;
const error: HTMLParagraphElement = document.querySelector("[data-error]")
formInput.addEventListener("submit", formHandler)

function formHandler(e) {
    e.preventDefault();

    if (e.target.value === "" || !emailReg.test(e.target.value) ) {
        error.setAttribute("data-error", "true")
    } else {
        error.setAttribute("data-error", "false")

    }
}